package com.example.rany.sqlitedemo.model;

import android.database.Cursor;

import com.example.rany.sqlitedemo.constant.DBConstant;

public class Article {

    private int id;
    private String title;
    private String desc;

    public Article(String title, String desc) {
        this.title = title;
        this.desc = desc;
    }

    public Article(int id, String title, String desc) {
        this.id = id;
        this.title = title;
        this.desc = desc;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Article{" + " " + id +
                " " + title + " " + desc + '}';
    }

    public static Article getArticleCursor(Cursor cursor){
        int id = cursor.getInt(cursor.getColumnIndex(DBConstant.ARTICLE_ID));
        String title = cursor.getString(cursor.getColumnIndex(DBConstant.ARTICLE_TITLE));
        String des = cursor.getString(cursor.getColumnIndex(DBConstant.ARTICLE_DESC));
        return new Article(id, title, des);
    }

}
