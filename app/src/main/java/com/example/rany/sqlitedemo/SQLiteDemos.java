package com.example.rany.sqlitedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rany.sqlitedemo.congrate_class.ArticleHelper;
import com.example.rany.sqlitedemo.model.Article;

import java.util.List;

public class SQLiteDemos extends AppCompatActivity {

    private ArticleHelper articleHelper;
    private EditText title, des, id;
    private Button insert, query, delete, get, update;
    private int a_id;
    private String a_title,a_des;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        articleHelper = ArticleHelper.getArticleHelper(this);

        initView();
        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Article article = new Article(title.getText().toString(),
                        des.getText().toString());
                if(articleHelper.insertArticle(article)){
                    Toast.makeText(SQLiteDemos.this, "Insert Successful", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(SQLiteDemos.this, "Failed !", Toast.LENGTH_SHORT).show();
                clearInput();
            }
        });
        query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Article> list = articleHelper.getArticleList();
               // Toast.makeText(SQLiteDemos.this, list.toString(), Toast.LENGTH_SHORT).show();
                Log.e("ooooo", list.toString());
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(articleHelper.deleteArticle(Integer.parseInt(id.getText().toString()))){
                    Toast.makeText(SQLiteDemos.this, "Deleted !", Toast.LENGTH_SHORT).show();
                }
                else Toast.makeText(SQLiteDemos.this, "Failed !", Toast.LENGTH_SHORT).show();
            }
        });
        get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Article article = articleHelper.getArticle(id.getText().toString());
                title.setText(article.getTitle());
                des.setText(article.getDesc());
                Log.e("ooooo", article.toString());
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initString();
                Article article = new Article(a_id, a_title, a_des);
                if (articleHelper.updateArticle(article)){
                    Toast.makeText(SQLiteDemos.this, "Success", Toast.LENGTH_SHORT).show();
                }
                else Toast.makeText(SQLiteDemos.this, "Fail", Toast.LENGTH_SHORT).show();
                clearInput();
            }
        });
    }

    private void initView() {
        title = findViewById(R.id.tvTitle);
        des = findViewById(R.id.tvDesc);
        insert = findViewById(R.id.btnInsert);
        query = findViewById(R.id.btnQuery);
        id = findViewById(R.id.tvId);
        delete = findViewById(R.id.btnDelete);
        get = findViewById(R.id.btnGetArticle);
        update = findViewById(R.id.btnUpdate);
    }

    public void initString(){
        a_id = Integer.parseInt(id.getText().toString());
        a_title = title.getText().toString();
        a_des = des.getText().toString();
    }

    public void clearInput(){
        title.setText("");
        des.setText("");
        id.setText("");
    }
}
