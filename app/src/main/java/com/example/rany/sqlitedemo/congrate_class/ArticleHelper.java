package com.example.rany.sqlitedemo.congrate_class;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.rany.sqlitedemo.constant.DBConstant;
import com.example.rany.sqlitedemo.model.Article;

import java.util.ArrayList;
import java.util.List;

public class ArticleHelper{

    private SQLiteDatabase sql;
    private DBHelper dbHelper;

    private static ArticleHelper articleHelper;
    public static ArticleHelper getArticleHelper(Context context){
        if(articleHelper == null){
            articleHelper = new ArticleHelper(context);
        }
        return articleHelper;
    }

    public ArticleHelper(Context context) {
        dbHelper = DBHelper.getInstance(context);
    }

    public boolean insertArticle(Article article){
        sql = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBConstant.ARTICLE_TITLE, article.getTitle());
        values.put(DBConstant.ARTICLE_DESC, article.getDesc());

        long qr = sql.insert(DBConstant.TABLE_ARTICLE, null, values);
        if(qr > 0){
            return true;
        }
        else return false;
    }

    public List<Article> getArticleList(){
        List<Article> articleList = new ArrayList<>();
        sql = dbHelper.getReadableDatabase();
        Cursor cursor = sql.query(DBConstant.TABLE_ARTICLE,
                null,
                null,
                null,
                null,
                null,
                null);
        // checking
        if (cursor != null && cursor.moveToFirst()){
            while (!cursor.isAfterLast()){
            Article article = Article.getArticleCursor(cursor);
            articleList.add(article);
            cursor.moveToNext();
            }
        }
        return articleList;
    }

    public boolean deleteArticle(int id){
        sql = dbHelper.getReadableDatabase();
        int delete = sql.delete(DBConstant.TABLE_ARTICLE,
                DBConstant.ARTICLE_ID + " = ? ",
                new String[]{String.valueOf(id)}
                );
        if(delete > 0){
            return true;
        }
        else
            return false;
    }

    public Article getArticle(String id) {

        Article article = null;

        sql = dbHelper.getReadableDatabase();
        Cursor cursor = sql.rawQuery("SELECT * FROM "
                        + DBConstant.TABLE_ARTICLE
                        + " WHERE " + DBConstant.ARTICLE_ID + " = ? ",
                new String[]{id});

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                article = Article.getArticleCursor(cursor);
                cursor.moveToNext();
            }

        }
        return article;
    }

    public boolean updateArticle(Article article){

        sql = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBConstant.ARTICLE_TITLE, article.getTitle());
        values.put(DBConstant.ARTICLE_DESC, article.getDesc());
        int update = sql.update(DBConstant.TABLE_ARTICLE,
                values,
                DBConstant.ARTICLE_ID + " = ? ",
                new String[]{String.valueOf(article.getId())}
                );
        if( update > 0){
            return true;
        }
        else
            return false;
    }

}