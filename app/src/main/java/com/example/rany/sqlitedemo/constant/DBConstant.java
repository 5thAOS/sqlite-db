package com.example.rany.sqlitedemo.constant;

public class DBConstant {

    public static final String DATABASE_NAME = "db_article";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_ARTICLE = "tb_article";
    public static final String ARTICLE_ID = "article_id";
    public static final String ARTICLE_TITLE = "article_title";
    public static final String ARTICLE_DESC = "article_desc";


    public static final String CREATE_TABLE_ARTICLE =
            "CREATE TABLE "+ TABLE_ARTICLE + " ( "
            + ARTICLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ARTICLE_TITLE + " TEXT NOT NULL, "
            + ARTICLE_DESC + " TEXT NOT NULL ) ";
}
